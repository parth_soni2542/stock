import React,{ Component } from 'react';
import {Card,Form,Button} from "tabler-react";
import axios from 'axios';



class Editsubcat extends Component{
constructor(props) {
	  super(props);
	  this.onChangesubategoryname = this.onChangesubategoryname.bind(this); 
	  this.onChangeOptionmenu = this.onChangeOptionmenu.bind(this);
	  // this.onSubmit = this.onSubmit.bind(this);
	
	  this.state = {
	  	 cat_names:[],
	  	 subcategory_name:'',
	  	 cat_id:'1',
	  	 sub_id:'1',
	  	 subcategorys:[],
	  	 option_error:'',
	  	 subcategory_error:''



	  };
	} 
	onChangesubategoryname(e){
		this.setState({
			subcategory_name:e.target.value,
			// cat_id:e.target.value
		});

	}
	onChangeOptionmenu(e){
		this.setState({
			cat_id:e.target.value

		})
		console.log(e.target.value)
	}




componentDidMount(){
	this.getcatecoryid();
	this.categoryFeatchdata();
	this.subcategoryDataFeatch();

}
	  categoryFeatchdata(){
			axios.get('http://localhost/React/subcategory.php')
			.then(response => {
				this.setState({cat_names: response.data });
			})
			.catch(function (error){
				console.log(error);
		});
		
}
subcategoryDataFeatch(){

	axios.get('http://localhost/React/subcategoryview.php')
  			.then(response => {
  				this.setState({subcategorys: response.data });
  				})
  			.catch(function (error){
  			// console.log(error);
  	});

} 
getcatecoryid(){
	console.log("subcategoryid: ",this.props.match.params.id)
	axios.get('http://localhost/React/getsubcat.php?id='+this.props.match.params.id)
	.then(response => {
		this.setState({
			subcategory_name:response.data.subcategory_name,
			category_name:response.data.category_name

		})
		// console.log(this.props.match.params.id);
		})
		.catch(function (error){
			console.log(error);
		})

	}

isSelected(a){
		if (a == this.props.match.params.id) {
			console.log(a, "selectedcategory");
			return true
		} else {
			console.log("category",a);
			return false
		}

	}
	render(){
		return(	<div className="p-5">
			<form onSubmit={this.onSubmit}>
			 <div className="form-group">
  			<label>Select Category</label>
  			<select className="form-control col-5" onChange={this.onChangeOptionmenu}>
  			
 
  			  {

  			 this.state.subcategorys.map(subcategorys  =>{
  			 	return (<option key={subcategorys.cat_id} value={subcategorys.cat_id} selected={this.isSelected(subcategorys.cat_id)}>{subcategorys.category_name}
  			</option>);
  			 })
  			}
  			</select>
  			<div style={{color: 'red'}}>{this.state.option_error}</div>
			</div>
			
			<div className="form-group">
				<input type="text" className="form-control col-5"
				 value={this.state.subcategory_name} onChange={this.onChangesubategoryname}
				 placeholder="SubcategoryName" /> 
				 <div style={{color: 'red'}}>{this.state.subcategory_error}</div>
			</div>
			
			<div className="form-group">
				<input type="submit" className="btn btn-primary"
						value="Update Subcategory" />
				</div>
			</form>

			<br>
</br>
</div>)
	}
}
export default Editsubcat;