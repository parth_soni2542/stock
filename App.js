import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.css';

import AddVender from './components/Addvender';
import Addbulding from './components/Addbulding';
import Addcategory from './components/Addcategory';
import Adduser from './components/Adduser';
import Addsubcategory from './components/Addsubcategory';
import Addproduct from './components/Addproduct';
import AddStock from './components/AddStock';
import Editcat from './components/Editcat';
import LoginPage from'./components/LoginPage';
import {Card,Nav } from "tabler-react";


function App() {
  return (
<Router>
  <Card>
      <Card.Header>
          <Card.Title>Stock Management</Card.Title>
      </Card.Header>
      <Card.Body>
<Nav>
  <Link to={'/'} className="nav-link" icon="user">Home</Link> 
  <Link to={'/Addcategory'} className="nav-link" icon="user">Addcategory</Link>            
  <Link to={'/Addsubcategory'} className="nav-link">Addsubcategory</Link>
  <Link to={'/Addproduct'} className="nav-link">Addproduct</Link>
  <Link to={'/Addvender'} className="nav-link">AddVender</Link>
  <Link to={'/Addbulding'} className="nav-link">AddBulding</Link>
  <Link to={'/Adduser'} className="nav-link">AddUser</Link>
  <Link to={'/AddStock'} className="nav-link">AddStock</Link>
  <Link to={'/LoginPage'} className="nav-link">LoginPage</Link>
</Nav>
      </Card.Body>
    </Card>
     <Switch>
      <Route exact path='/Addvender' component={AddVender} />
      <Route path='/Addbulding' component={Addbulding} />
      <Route path='/Addcategory' component={Addcategory} />
      <Route path='/Adduser' component={Adduser} />
      <Route path='/Addsubcategory' component={Addsubcategory} />
      <Route path='/Addproduct' component={Addproduct} />
      <Route path= '/Editcat/:id' component={Editcat} />
      <Route path='/AddStock' component={AddStock} />
      <Route path='/LoginPage' component={LoginPage} />
     </Switch> 
    </Router>
  );
}
export default App;
